# Component Combos

A list of custom react polaris components.

## Getting started

### Installation

Install all the project dependencies.

```bash
npm install
```

### Development

Run the local `create-react-app` development server.

```bash
npm start
```

Open **http://localhost:3000** in your browser and you should see the application.
